﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace prog2500ass1 {
    public class Player {

        private NewCharacter playerChar;
        private int width = 20;
        private int height = 25;
        private int x = 0;
        private int y = 0;
        private int rowNum = 0;
        private int colNum = 0;

        // ===================================================
        
        //Default Constructor --------------------------------
        //No Char Choices
        public Player() {
            

        } //end constructor


        //Overloaded Constructor -----------------------------
        //Chosen Char Choices
        public Player(int choice) {
            
            if(choice==1) {
                this.playerChar = new Assasin();
                        
            } else if(choice==2) {
                this.playerChar = new Elite();
            
            } else if(choice==3) {
                this.playerChar = new Guardian();            
            
            }

        } //end constructor

        // ===================================================


        //Functions ------------------------------------------
        private int generateRand(int newNum, bool hasZero) {
	        Random r = new Random();
	        int rand = r.Next(newNum);
		
	        if(rand==0 && hasZero==false) {
		        rand++;
	        }
		
	        return rand;

        } //end function

        public void drawPlayer(System.Windows.Forms.PaintEventArgs e) {                       
            Image image = prog2500ass1.Properties.Resources.noArm;
            e.Graphics.DrawImage(image, this.x, this.y, this.width, this.height);
        
        } //end function

        


        //Setters -----------------------------------------
        public void setWidth(int newWidth) {
            this.width = newWidth;

        } //end function

        public void setheight(int newHeight) {
            this.height = newHeight;

        } //end function

        public void setX(int newX) {
            this.x = newX + 13;

        } //end function

        public void setY(int newY) {
            this.y = newY + 7;

        } //end function

        public void setRowNum(int newRow) {
            this.rowNum = newRow;
        
        } //end function

        public void setColNum(int newCol) {
            this.colNum = newCol;
        
        } //end function





        //Getters -----------------------------------------
        public int getWidth() {
            return this.width;

        } //end function

        public int getHeight() {
            return this.height;

        } //end function

         public int getX() {
            return this.x;

        } //end function
        
        public int getY() {
            return this.y;

        } //end function

        public int getRowNum() {
            return this.rowNum;

        } //end function

        public int getColNum() {
            return this.colNum;        
        
        } //end function

        public NewCharacter getCharType() {
            return this.playerChar;
        
        } //end function




    } //EOC

} //EON
