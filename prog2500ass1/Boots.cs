﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    public class Boots : Armor {
	
	    private String[,] boots = {{"Plated Boots","Soft Leather Muters","Steel Woven Stomps"},
								    {"Bone Fused Boots","Thin Leather Muters","Steel-Fused Stomps"},
								    {"Ivory Linked Boots","Shadow Mist Forms","Plated Iron Marchers"},
								    {"Exultic Boots","Ascthic Shadow Forms","Guthoric Great Clodids"},
								    {"Legendary Exum Boots","Legendary Ixid Forms","Legendary AshipHa Clodids"},
							       };


	    String currentBoots;
	
	    public Boots(String charType) {
		    validateCharUsingWeapon(charType);
	    }
	
	    private void validateCharUsingWeapon(String charT) {
		    if(charT.Equals("assasin")) {
			    currentBoots = boots[0, 1];
		    }else if(charT.Equals("elite")) {
			    currentBoots = boots[0, 0];
		    }else if(charT.Equals("guardian")) {
			    currentBoots = boots[0, 2];
		    }
	    }
	
	    public String getBootsName() {
		    return currentBoots;
	    }


    }
}
