﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    public abstract class Weapon : StatCalculator {

	    //Start Timer for bind procedure
	    TimerStorage bindEvent = new TimerStorage();
	
	    private int attackPower = 0;
	    private int wepStr = 1;
	    private int chanceOfStoneBind = 3;
	    private int startOfChanceRange = 0;
	    private int endOfChanceRange = 100;
		
	
	    public Weapon() {
		    attackPower += attackPowerCalculator(wepStr, "GEAR");
		
	    }//end of constructors
	
	
	    private double getChanceOfBind() {
		    double bindChance = endOfChanceRange - startOfChanceRange;
		    bindChance /= chanceOfStoneBind;
		    bindChance = (double)Math.Round(bindChance);
		
		    return bindChance;
	    }
	
		
	
	
	
	
	    public int getWepAttack() {
		    return attackPower;
	    }
	
	    public void addWepAttack(int inAttk) {
		    attackPower += inAttk;
	    }
		
	    public void setWepAttack(int inAttk) {
		    attackPower = inAttk;
	    }
	
	    public int equipSword() {
		    return attackPower;
	    }
	
	    public void addWepStr(int inStr) {
		    wepStr += inStr;
	    }
	
	    public int getWepStr() {
		    return wepStr;
	    }
		
	    public void bindStone() {
		
		
		
		
	    }
	
	
    }
}
