﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace prog2500ass1 {
    public class Room {
        
        //Private Members ----------------------------------
        private bool visited = false;
        private bool door = false;
        private bool key = false;
        


        //Setters -----------------------------------------
        public void setVisitedTrue() {
            this.visited = true;

        } //end function

        public void setVisitedFalse() {
            this.visited = true;

        } //end function

        public void setDoorFalse() {
            this.door = false;

        } //end function

        public void setDoorTrue() {
            this.door = true;

        } //end function

        public void setKeyFalse() {
            this.key = false;

        } //end function

        public void setKeyTrue() {
            this.key = true;

        } //end function




        //Getters -----------------------------------------
        public bool getVisitedState() {
            return this.visited;
        
        } //end function

         public bool getHasDoor() {
            return this.door;
        
        } //end function

         public bool getHasKey() {
            return this.key;
        
        } //end function




        //Functions -----------------------------------------
        public void swapVisitedStates() {
            if (!this.visited) {
                this.visited = true;
            } else {
                this.visited = false;
            }

        } //end function

       
    } //EOC

} //EON
