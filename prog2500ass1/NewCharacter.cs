﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    public abstract class NewCharacter : StatCalculator {

	    String name;
	    String choosenCharType;
	
	    //Base Stats regardless of char type
	    private int str = 5;
	    private int def = 5;
	    private int HP = 500;
	    private int block = 25;
	    private int stealth = 1;
	    private int evade = 25;
	    private int attack = 100;
	    private int attackChance = 0;
	    private int EXP = 0;
	    private int EXPcap = 500;
	    private int lvlPts = 0;
	    private int ptsPerLvl = 7;
	
	    //Dynamic in-game stats
	    private int onGoingHp = 0;
	
	    //Overloaded constructor
	    public NewCharacter(String charType,int charStr,int charDef,int charStealth,int charAttack) {
		    str += charStr;
		    def += charDef;
		    stealth += charStealth;
		    attack += charAttack; 
		    attack += attackPowerCalculator(str,"PLAYER");
		    block += defenseCalculator(def, "PLAYER");
		    evade += calculateChanceOfEvade(stealth,"PLAYER");
		    attackChance += calculateChanceOfHit();
		    onGoingHp = HP;
		
		    choosenCharType = charType;
	    }
		
	
	    //                               ********STR section*********
	
	    //Start method to allow program to add to str
	    public void addStr(int addStr) {
		    str += addStr;
	    }
	
	    //Start method to completely change str
	    public void changeStrValue(int newStr) {
		    str = newStr;
	    }
	
	    //Start method to return str amount
	    public int getStr() {
		    return str;
	    }
	
	
	    // 								 ********Def section*********
		
	    //Start method to allow program to add to def
	    public void addDef(int addDef) {
		    def += addDef;
	    }
	
	    //Start method to completely change def
	    public void changeDefValue(int newDef) {
		    def = newDef;
	    }
	
	    //Start method to return def amount
	    public int getDef() {
		    return def;
	    }
	
	    //	 							********Attack section*********
		
	    //Start method to allow program to add to attack
	    public void addAtk(int addAtk) {
		    attack += addAtk;
	    }
	
	    //Start method to completely change attack
	    public void changeAtkValue(int newAtk) {
		    def = newAtk;
	    }
	
	    //Start method to return attack amount
	    public int getAtk() {
		    return attack;
	    }
	
        //                               ********-HP section*********
	
	    //Start method to allow program to add to HP
	    public void addHP(int addHP) {
		    HP += addHP;
	    }
		
	    //Start method to completely change HP
	    public void changeHP(int newHP) {
		    HP = newHP;
	    }
	
	    //Start method to return HP amount
	    public int getHP() {
		    return HP;
	    }
	
	    //start method to deal damage
	    public void doDmg(int inDmg) {
		    onGoingHp -= inDmg;
	    }
	
	
	    // 								 ********-Stealth section*********
		
	    //Start method to allow program to add to Stealth
	    public void addStealth(int addStealth) {
		    stealth += addStealth;
	    }
	
	    //Start method to completely change Stealth
	    public void changeStealth(int newStealth) {
		    stealth = newStealth;
	    }
	
	    //Start method to return Stealth amount
	    public int getStealth() {
		    return stealth;
	    }
	
	
	    //							    ********-LVL section*********
	
	    //Start method to add a lvl
	    public void lvlUp() {
		    level ++;
		    EXPcap += 500;
		    attackPowerCalculator(str, "PLAYER");
		    attackChance = calculateChanceOfHit();
	    }
	
	    //Start method to allow program to add to lvl
	    public void addNumOfLvls(int addLvls) {
		    level += addLvls;
	    }
	
	    //Start method to completely change lvl
	    public void changeLevel(int newLvl) {
		    level = newLvl;
	    }
	
	    //Start method to return lvl amount
	    public int getLvl() {
		    return level;
	    }
	
	    //start method to return EXPCap
	    public int getLvlCap() {
		    return EXPcap;
	    }
	
	
	    //						        ********-EXP section*********
	
	    //Start method to allow program to add to EXP
	    public void addEXP(int addEXP) {
		    EXP += addEXP;
	    }
	
	    //Start method to completely change EXP
	    public void changeEXP(int newEXP) {
		    EXP = newEXP;
	    }
	
	    //Start method to return EXP amount
	    public int getEXP() {
		    return EXP;
	    }
	
	
	
	    //						        ********-Misc section*********			
	
	    public String getName() {
		    return name;
	    }
	
	    public void setName(String n) {
		    name = n;
	    }
	
	    public void setLvlPts(int n) {
		    lvlPts = n;
	    }
	
	    public int getLvlPts() {
		    return lvlPts;
	    }
	
	    public void setCurrentGameHp(int num) {
		    onGoingHp = num;
	    }
	
	    public int getOnGoingGameHp() {
		    return onGoingHp;
	    }
	
	    public void addToBlock(int inBlock) {
		    block += inBlock;
	    }
	
	    public int getChanceOfAttack() {
		    return attackChance;
	    }

    } // EOC

} //EON
