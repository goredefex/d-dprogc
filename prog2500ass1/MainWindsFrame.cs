﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prog2500ass1 {
    public partial class MainWindsFrame : Form {

        //Game Pieces
        private Arena arena;
        private Player player;


        //Default Constructor ---------------------------
        public MainWindsFrame() {
            InitializeComponent();

            //Create Default Player And Empty Arena
            this.player = new Player();
            this.arena = new Arena();
            this.displayMsg("Welcome To The Winds Of Change");
            

        } //end constructor

        // =============================================




        //Functions ----------------------------------------------------------
        public void displayMsg(String newMsg) {
            this.messageZoneTextBox.AppendText(newMsg + "\n");
        
        } //end function

        public void addToInventory(String newItem) {
            this.inventoryListBox.Items.Add(newItem);
        
        } //end function

        public void retrieveKey() {
            this.displayMsg("You Found A Key In Level " + this.arena.getCurrentLvl());
            MessageBox.Show ("You Found A Key In Level " + this.arena.getCurrentLvl(), 
                "The Winds Of Change", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);

            this.addToInventory("Level " + this.arena.getCurrentLvl() + " Key");
        
        } //end function

        public void setRoomUncovered() {
            this.arena.visitRoom(this.player.getRowNum(), this.player.getColNum());
                    
            //Paint Room
            this.arena.setRoomColorProp(this.player.getRowNum(), this.player.getColNum(), "white");
            this.arenaVisualPanel.Refresh();

        } //end function

        public void attachDoorToBoard() {
            this.arenaVisualPanel.Paint += this.levelDoor_Paint;
            this.arenaVisualPanel.Refresh();

        } //end function


       


        //Paint Events --------------------------------------------------------
        private void arenaVisualPanel_Paint(object sender, PaintEventArgs e) {
            this.arena.drawRoomArray(e);

        } //end event

        private void playerChar_Paint(object sender, PaintEventArgs e) {
            this.player.drawPlayer(e);
        
        } //end event

        private void levelDoor_Paint(object sender, PaintEventArgs e) {
            this.arena.drawDoor(e);
        
        } //end event





        //Form Events --------------------------------------------------------
        // ===================================================================


        //Event of Exit Game Button Click
        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            Application.Exit();

        } //end event


        //Event of New Game Button Click
        private void newToolStripMenuItem_Click(object sender, EventArgs e) {
            
            string input = Microsoft.VisualBasic.Interaction.InputBox("Would You Like To Play as...\n\nAssasin - Stealth with Attack [1] \n"+
                                                                      "Elite - Attack with Strength [2] \nGuardian - Defense with HP [3] \n\n"+
                                                                      "Please pick a number and enter below...\n", "Pick A Number for a Charater", "1", 0, 0);
            //Char Choice Validation
            try {

                //Block For New game Creation On Running Game
                if(this.arena.getGameRunState() == false) {
                    int choice = Int16.Parse(input);

                    //Populate Player Choice To Player Obj
                    this.player = new Player(choice);
                    this.arena = new Arena(this.player);
                    this.arena.setRoomColorProp(this.player.getRowNum(), this.player.getColNum(), "white");
                    this.displayMsg("New Game Started! You Picked Type - " + this.player.getCharType().getName());
                    this.arenaVisualPanel.Paint += this.playerChar_Paint;
                    this.arenaVisualPanel.Refresh();
                
                    //Launch Game Object
                    GameDriver newGame = new GameDriver();
                    newGame.start(this.arena);
   
                }
            
            } catch(Exception ex) {
                Console.WriteLine(ex.Message);

            }  

        } //end event

        //Event of Char Stats Button Click
        private void actionBtbStats_Click(object sender, EventArgs e) {
            CharStatsFrame statsFrame = new CharStatsFrame();
            statsFrame.Show();

        } //end event



        //Movement Buttons -------------------------------------
        private void moveBtnUp_Click(object sender, EventArgs e) {
            if(this.arena.getGameRunState()) {
                if(this.arena.getPlayerLocation()[0] == 0) {
                    this.displayMsg("You Cannot Move Outside The Arena!");
            
                } else {
                    this.arena.moveRoomUp();
                    this.setRoomUncovered();

                    if(this.arena.getKeyLocation()[0]==this.player.getRowNum() && this.arena.getKeyLocation()[1]==this.player.getColNum()) {
                        this.retrieveKey();
                    }

                    if(this.arena.getDoorLocation()[0]==this.player.getRowNum() && this.arena.getDoorLocation()[1]==this.player.getColNum()) {
                        this.attachDoorToBoard();
                    }
            
                }

            }
            
        } //end event

        private void moveBtnDown_Click(object sender, EventArgs e) {
            if(this.arena.getGameRunState()) {
                if(this.arena.getPlayerLocation()[0] == (this.arena.getRows()-1)) {
                    this.displayMsg("You Cannot Move Outside The Arena!");
            
                } else {
                    this.arena.moveRoomDown();
                    this.setRoomUncovered();

                    if(this.arena.getKeyLocation()[0]==this.player.getRowNum() && this.arena.getKeyLocation()[1]==this.player.getColNum()) {
                        this.retrieveKey();
                    }

                    if(this.arena.getDoorLocation()[0]==this.player.getRowNum() && this.arena.getDoorLocation()[1]==this.player.getColNum()) {
                        this.attachDoorToBoard();
                    }
            
                }

            }

        } //end event

        private void moveBtnRight_Click(object sender, EventArgs e) {
            if(this.arena.getGameRunState()) {
                if(this.arena.getPlayerLocation()[1] == (this.arena.getCols()-1)) {
                    this.displayMsg("You Cannot Move Outside The Arena!");
            
                } else {
                    this.arena.moveRoomRight();
                    this.setRoomUncovered();

                    if(this.arena.getKeyLocation()[0]==this.player.getRowNum() && this.arena.getKeyLocation()[1]==this.player.getColNum()) {
                        this.retrieveKey();
                    }

                    if(this.arena.getDoorLocation()[0]==this.player.getRowNum() && this.arena.getDoorLocation()[1]==this.player.getColNum()) {
                        this.attachDoorToBoard();
                    }
            
                }

            }

        } //end event

        private void moveBtnLeft_Click(object sender, EventArgs e) {
            if(this.arena.getGameRunState()) {
                if(this.arena.getPlayerLocation()[1] == 0) {
                    this.displayMsg("You Cannot Move Outside The Arena!");
            
                } else {
                    this.arena.moveRoomLeft();
                    this.setRoomUncovered();

                    if(this.arena.getKeyLocation()[0]==this.player.getRowNum() && this.arena.getKeyLocation()[1]==this.player.getColNum()) {
                        this.retrieveKey();
                    }

                    if(this.arena.getDoorLocation()[0]==this.player.getRowNum() && this.arena.getDoorLocation()[1]==this.player.getColNum()) {
                        this.attachDoorToBoard();
                    }
            
                }

            }

        }

        private void actionBtnInteract_Click(object sender, EventArgs e) {
            

        } //end event



    } //EOC

} //EON
