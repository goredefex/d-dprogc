﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    public class BodyArmor : Armor {

	    private String[,] bodyArmor = {{"Platemail","Leather Breastplate","Steel Woven Chainmail"},
									    {"Bone Fused Chestplate","Steel-Enforced Leather Breastplate","Steel-Fused Broadmail"},
									    {"Ivory Graphted Chestplate","Shadow Mist Breastplate","Plated Iron Broadmail"},
									    {"Exultic Chestplate","Ascthic Shadow Plate","Guthoric Great Broadmail"},
									    {"Legendary Exum Chestplate","Legendary Ixid Plate","Legendary AshipHa Broadmail"},
								       };


	    String currentBodyArmor;
	
	    public BodyArmor(String charType) {
		    validateCharUsingWeapon(charType);
	    }
	
	    private void validateCharUsingWeapon(String charT) {
		    if(charT.Equals("assasin")) {
			    currentBodyArmor = bodyArmor[0, 1];
		    }else if(charT.Equals("elite")) {
			    currentBodyArmor = bodyArmor[0, 0];
		    }else if(charT.Equals("guardian")) {
			    currentBodyArmor = bodyArmor[0, 2];
		    }
	    }
	
	    public String getBodyArmorName() {
		    return currentBodyArmor;
	    }


	
    }
}
