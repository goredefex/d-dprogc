﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace prog2500ass1 {
    public class Arena {
        
        //Board Square Size - [n] X [n]
        private const int cubeSize = 5;

        //Control Objects
	    private static TimerStorage eventOccurence = new TimerStorage();
        private GameItems items = new GameItems();

        //Game Bounds
        private int rows = 0;
        private int cols = 0;
        private Player currentPlayer;
        
        //Arena
        private PhysicalRoom[,] gameArena = new PhysicalRoom[cubeSize, cubeSize];
        
        //Locations
	    private int[] keyLocation;
	    private int[] doorLocation;
        private int[] playerLocation;

        //Odds Are 1 In (n) Chances
	    private int battleChance = 5;
	    private int itemChance = 10;
	    private int gearChance = 5;

        //Game controllers
	    private int turn = 1;
	    private int level = 1;
        private bool gameRunning = false;

        //Timer event objects & chance sizes
	    private int startOfChanceRange = 0;
	    private int finalOfChanceRange = 100;
        


        //Default Constructor -------------------------------
        public Arena() { 
            
            //Discover Program Bounds
            this.rows = gameArena.GetLength(0);
            this.cols = gameArena.GetLength(1);

            //Create Rooms~
            this.instantiateArena();
            
        } //end constructor


        //Overloaded Constructor -----------------------------
        public Arena(Player player) {  

            //Discover Program Bounds
            this.rows = gameArena.GetLength(0);
            this.cols = gameArena.GetLength(1);

            //Create Rooms~
            this.instantiateArena();

            //Setup Key & Door Positions
            this.keyLocation = new int[2] {generateRand(this.rows, true), generateRand(this.cols, true)};
            this.doorLocation = new int[2] {generateRand(this.rows,true), generateRand(this.cols,true)};

            //Ensure Key & Door originality
            this.keyLocation = this.validateAlike(this.keyLocation, this.doorLocation);

            //Setup Player Starting Position
            this.playerLocation = new int[2] {generateRand(this.rows, true), generateRand(this.cols, true)};

            //Insert Player Into Arena
            this.currentPlayer = player;
            this.updatePlayerCoordinates();
            this.currentPlayer.setRowNum(this.playerLocation[0]);
            this.currentPlayer.setColNum(this.playerLocation[1]);

            //Run Game~
            this.gameRunning = true;


        } //end constructor            

        // ================================================



        //Private Functions ===============================
        private int[] validateAlike(int[] key, int[] door) {
		    if(key[0]==door[0] && key[1]==door[1]) {
			    if(key[0]==0) {
				    key[0]++;
			    }else if(key[0]==4) {
				    key[0]--;
			    }//end if
			
		    }//end if
		
		    return key;
        
        } //end function

        private int calcChances(int givenOdds) {
		    int calc = finalOfChanceRange - startOfChanceRange;
		    calc /= givenOdds;
		
		    return calc;

	    } //end function

        private int[] validateRangeException(int[] numberSet,int chances) {
		    if(numberSet[1]>=finalOfChanceRange) {
			    numberSet[0] = 0;
			    numberSet[1] = numberSet[0] + calcChances(chances);
		    }		
				
		    return numberSet;

	    } //end function

        private int generateRand(int newNum, bool hasZero) {
	        Random r = new Random((int) DateTime.Now.Ticks & 0x0000FFFF);
            int rand = r.Next(newNum);
		
	        if(rand==0 && hasZero==false) {
		        rand++;
	        }
		
	        return rand;

        } //end function

        private void updatePlayerCoordinates() {
            this.currentPlayer.setX(this.gameArena[this.playerLocation[0], this.playerLocation[1]].getX());
            this.currentPlayer.setY(this.gameArena[this.playerLocation[0], this.playerLocation[1]].getY());
        
        } //end function

        //=================================================


        //Public Functions ---------------------------------------
        public void instantiateArena() {
            int xPos = 20;
            int yPos = 5;
            for(int outer=0; outer <= (this.rows-1); outer++) {
			    for(int inner=0; inner <= (this.cols-1); inner++) {
				    this.gameArena[outer, inner] = new PhysicalRoom(xPos, yPos);
                    xPos += 60;
			    }	
                yPos += 45;
                xPos = 20;
		    }
        
        } //end function


        public void drawRoomArray(System.Windows.Forms.PaintEventArgs e) {
            for(int i=0; i<this.rows; i++) {
                for(int j=0; j<this.cols; j++) {
                    if(this.gameArena[i, j].getRoomColor().Equals("black")) {
                        this.gameArena[i, j].drawRoom(e, "black");

                    } else if(this.gameArena[i, j].getRoomColor().Equals("white")) {
                        this.gameArena[i, j].drawRoom(e, "white");
                    
                    }
                    
                }
            }
        
        } //end function


        public void drawDoor(System.Windows.Forms.PaintEventArgs e) {
            Image image = prog2500ass1.Properties.Resources.door;
            e.Graphics.DrawImage(image, this.gameArena[this.doorLocation[0], this.doorLocation[1]].getX(), 
                this.gameArena[this.doorLocation[0], this.doorLocation[1]].getY(), 35, 30);
        
        } //end function


        public bool checkForItemInRoom(int[] inRoom, out String itemNameFound) {
            bool itemRecieved = false;
            itemNameFound = String.Empty;

            if(turn>1) {
                    int itemStart = this.generateRand(finalOfChanceRange, false);
                    int[] itemSpecificOdds = {itemStart, itemStart + calcChances(itemChance)};
                    itemSpecificOdds = this.validateRangeException(itemSpecificOdds,itemChance);
							
                    //adding items	
                    if(eventOccurence.getCounter()>=itemSpecificOdds[0] && eventOccurence.getCounter()<=itemSpecificOdds[1]) {
                        itemRecieved = true;
                        String tempItem = items.getRandItem();
                        itemNameFound = tempItem;
					
                    }//end chance if
                }//end turn if
		
            return itemRecieved;

        }//end method


        public void newArena() {
		    this.keyLocation[0] = generateRand(this.rows-1,true);
		    this.keyLocation[1] = generateRand(this.cols-1,true);
		    this.doorLocation[0] = generateRand(this.rows-1,true);
		    this.doorLocation[1] = generateRand(this.cols-1,true);
		    this.keyLocation = validateAlike(keyLocation, doorLocation);
		    this.turn = 1;
		    this.resetArenaArray();		

	    } //end function


        public void resetArenaArray() {
            for(int outer=0; outer == this.rows-1; outer++) {
			    for(int inner=0; inner == this.cols-1; inner++) {
				    this.gameArena[outer, inner] = null;
			    }	
		    }
        
        } //end function


        public void moveRoomUp() {
            this.playerLocation[0]--;
            this.currentPlayer.setRowNum(this.playerLocation[0]);
            this.updatePlayerCoordinates();
            this.turn++;
        
        } //end function
        

        public void moveRoomDown() {
            this.playerLocation[0]++;
            this.currentPlayer.setRowNum(this.playerLocation[0]);
            this.updatePlayerCoordinates();
            this.turn++;
        
        } //end function


        public void moveRoomRight() {
            this.playerLocation[1]++;
            this.currentPlayer.setColNum(this.playerLocation[1]);
            this.updatePlayerCoordinates();
            this.turn++;
        
        } //end function


        public void moveRoomLeft() {
            this.playerLocation[1]--;
            this.currentPlayer.setColNum(this.playerLocation[1]);
            this.updatePlayerCoordinates();
            this.turn++;
        
        } //end function


        public void visitRoom(int currRow, int currCol) {
            this.gameArena[currRow, currCol].setVisitedTrue();
        
        } //end function


        public void resetTurnCounter() {
		    this.turn = 1;

	    } //end function


        public void floorLvlUp() {
		    this.level++;

	    } //end function
	

	    public void nextTurn() {
		    this.turn++;

	    } //end function
        
        
        
        //Setters ----------------------------------------
        public void setRows(int newRowSize) {
            this.rows = newRowSize;

        } //end function


        public void setCols(int newColSize) {
            this.cols = newColSize;

        } //end function


        public void setGameToRunning() {
            this.gameRunning = true;
        
        } //end function


        public void setGameToStopped() {
            this.gameRunning = false;
        
        } //end function


        public void setRoomColorProp(int rowX, int colY, String newColor) {
            if(newColor.ToLower().Equals("black")) {
                this.gameArena[rowX, colY].setRoomColorBlack();

            } else if(newColor.ToLower().Equals("white")) {
                this.gameArena[rowX, colY].setRoomColorWhite();

            }            
        
        } //end function



        //Getters ----------------------------------------
        public int getRows() {
            return this.rows;

        } //end function


        public int getCols() {
            return this.cols;

        } //end function
        

        public int getCurrentLvl() {
		    return level;

	    } //end function


        public int[] getKeyLocation() {
            return this.keyLocation;
        
        } //end function


        public int[] getPlayerLocation() {
            return this.playerLocation;
        
        } //end function


        public int[] getDoorLocation() {
            return this.doorLocation;
        
        } //end function


        public bool getGameRunState() {
            return this.gameRunning;
        
        } //end function


    } //EOC

} //EON
