﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    public class Leggings : Armor {

	    private String[,] leggings = {{"Plated Greaves","Leather Leggings","Steel Woven Blockers"},
								       {"Bone Fused Greaves","Steel-Enforced Leather Leggings","Steel-Fused Blockers"},
								       {"Ivory Linked Greaves","Shadow Mist Forms","Plated Iron Breachers"},
								       {"Exultic Greaves","Ascthic Shadow Forms","Guthoric Great Breachers"},
								       {"Legendary Exum Greaves","Legendary Ixid Forms","Legendary AshipHa Breachers"},
								      };


	    String currentLeggings;
	
	    public Leggings(String charType) {
		    validateCharUsingWeapon(charType);
	    }
	
	    private void validateCharUsingWeapon(String charT) {
		    if(charT.Equals("assasin")) {
			    currentLeggings = leggings[0, 1];
		    }else if(charT.Equals("elite")) {
			    currentLeggings = leggings[0, 0];
		    }else if(charT.Equals("guardian")) {
			    currentLeggings = leggings[0, 2];
		    }
	    }
	
	    public String getLegsName() {
		    return currentLeggings;
	    }

	
    }
}
