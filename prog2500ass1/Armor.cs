﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    public abstract class Armor : StatCalculator {

	    //Start Timer for bind procedure
	    TimerStorage bindEvent = new TimerStorage();
		
	    private int defense = 0;
	    private int armorHP = 50;    
	
	    private int chanceOfStoneBind = 3;
	    private int startOfChanceRange = 0;
	    private int endOfChanceRange = 100;
	
	    public Armor() {
		
		
	    }//end of constructors
				
	    private double getChanceOfBind() {
		    double bindChance = endOfChanceRange - startOfChanceRange;
		    bindChance /= chanceOfStoneBind;
		    bindChance = (double)Math.Round(bindChance);
			
		    return bindChance;
	    }
		
	
	
		
	    public int getArmorBlock() {
		    return defenseCalculator(defense, "GEAR");
	    }
	
	    public int getArmorHP() {
		    return armorHP;
	    }
			
	    public void armLvlUp(int inStr) {
		    gearLvl++;
				
	    }
	
	    public void addToArmHP(int inHP) {
		    armorHP += inHP;
	    }
			
	    public void bindStone() {
			
			
			
			
	    }

		
    }
}
