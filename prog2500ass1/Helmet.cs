﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    public class Helmet : Armor {

	    private String[,] helmets = { {"Plate Helmet","Leather Helmet","Heavy Helmet"},
								      {"Bone Helmet","Enforced Leather Helmet","Steel Helmet"},
								      {"Ivory Helmet","Woven Iron Helmet","Plated Iron Helmet"},
								      {"Exultic Helmet","Ascthic Bascinet","Guthoric Great Helm"},
								      {"Legendary Exum Helmet","Legendary Ixid Bascinet","Legendary AshipHa Great Helm"},
								     };
	
	
	    String currentHelmet;
	    //int helmetDef = 
	
	
	    public Helmet(String charType) {
		    validateCharUsingWeapon(charType);
	    }
	
	    private void validateCharUsingWeapon(String charT) {
		    if(charT.Equals("assasin")) {
			    currentHelmet = helmets[0, 1];
		    }else if(charT.Equals("elite")) {
			    currentHelmet = helmets[0, 0];
		    }else if(charT.Equals("guardian")) {
			    currentHelmet = helmets[0, 2];
		    }
	    }
	
	    public String getHelmetName() {
		    return currentHelmet;
	    }
	
    }
}
