﻿namespace prog2500ass1
{
    partial class MainWindsFrame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gameStaticMenuStrip = new System.Windows.Forms.MenuStrip();
            this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveGameAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gameMenuStripDropdown = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainGameSplitContainer = new System.Windows.Forms.SplitContainer();
            this.arenaMovePanel = new System.Windows.Forms.SplitContainer();
            this.arenaGroupBox = new System.Windows.Forms.GroupBox();
            this.arenaVisualPanel = new System.Windows.Forms.Panel();
            this.MovementsGroupBox = new System.Windows.Forms.GroupBox();
            this.moveBtnLeft = new System.Windows.Forms.Button();
            this.moveBtnRight = new System.Windows.Forms.Button();
            this.moveBtnDown = new System.Windows.Forms.Button();
            this.moveBtnUp = new System.Windows.Forms.Button();
            this.inventoryCharSplitPanel = new System.Windows.Forms.SplitContainer();
            this.actionCharPicSplitContainer = new System.Windows.Forms.SplitContainer();
            this.actionsGroupBox = new System.Windows.Forms.GroupBox();
            this.actionBtnEquip = new System.Windows.Forms.Button();
            this.actionBtnDrop = new System.Windows.Forms.Button();
            this.actionBtbStats = new System.Windows.Forms.Button();
            this.actionBtnInteract = new System.Windows.Forms.Button();
            this.InventoryGroupBox = new System.Windows.Forms.GroupBox();
            this.inventoryListBox = new System.Windows.Forms.ListBox();
            this.eventsGroupBox = new System.Windows.Forms.GroupBox();
            this.messageZoneTextBox = new System.Windows.Forms.RichTextBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.gameStaticMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGameSplitContainer)).BeginInit();
            this.mainGameSplitContainer.Panel1.SuspendLayout();
            this.mainGameSplitContainer.Panel2.SuspendLayout();
            this.mainGameSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.arenaMovePanel)).BeginInit();
            this.arenaMovePanel.Panel1.SuspendLayout();
            this.arenaMovePanel.Panel2.SuspendLayout();
            this.arenaMovePanel.SuspendLayout();
            this.arenaGroupBox.SuspendLayout();
            this.MovementsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryCharSplitPanel)).BeginInit();
            this.inventoryCharSplitPanel.Panel1.SuspendLayout();
            this.inventoryCharSplitPanel.Panel2.SuspendLayout();
            this.inventoryCharSplitPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.actionCharPicSplitContainer)).BeginInit();
            this.actionCharPicSplitContainer.Panel1.SuspendLayout();
            this.actionCharPicSplitContainer.Panel2.SuspendLayout();
            this.actionCharPicSplitContainer.SuspendLayout();
            this.actionsGroupBox.SuspendLayout();
            this.InventoryGroupBox.SuspendLayout();
            this.eventsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // gameStaticMenuStrip
            // 
            this.gameStaticMenuStrip.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.gameStaticMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameToolStripMenuItem});
            this.gameStaticMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.gameStaticMenuStrip.Name = "gameStaticMenuStrip";
            this.gameStaticMenuStrip.Size = new System.Drawing.Size(959, 29);
            this.gameStaticMenuStrip.TabIndex = 1;
            this.gameStaticMenuStrip.Text = "menuStrip1";
            // 
            // gameToolStripMenuItem
            // 
            this.gameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.loadGameToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveGameAsToolStripMenuItem,
            this.gameMenuStripDropdown,
            this.exitToolStripMenuItem});
            this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
            this.gameToolStripMenuItem.Size = new System.Drawing.Size(63, 25);
            this.gameToolStripMenuItem.Text = "Game";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // loadGameToolStripMenuItem
            // 
            this.loadGameToolStripMenuItem.Name = "loadGameToolStripMenuItem";
            this.loadGameToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.loadGameToolStripMenuItem.Text = "Load Game";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // saveGameAsToolStripMenuItem
            // 
            this.saveGameAsToolStripMenuItem.Name = "saveGameAsToolStripMenuItem";
            this.saveGameAsToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.saveGameAsToolStripMenuItem.Text = "Save Game As...";
            // 
            // gameMenuStripDropdown
            // 
            this.gameMenuStripDropdown.Name = "gameMenuStripDropdown";
            this.gameMenuStripDropdown.Size = new System.Drawing.Size(185, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // mainGameSplitContainer
            // 
            this.mainGameSplitContainer.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.mainGameSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGameSplitContainer.IsSplitterFixed = true;
            this.mainGameSplitContainer.Location = new System.Drawing.Point(0, 29);
            this.mainGameSplitContainer.Name = "mainGameSplitContainer";
            // 
            // mainGameSplitContainer.Panel1
            // 
            this.mainGameSplitContainer.Panel1.Controls.Add(this.arenaMovePanel);
            // 
            // mainGameSplitContainer.Panel2
            // 
            this.mainGameSplitContainer.Panel2.Controls.Add(this.inventoryCharSplitPanel);
            this.mainGameSplitContainer.Size = new System.Drawing.Size(959, 454);
            this.mainGameSplitContainer.SplitterDistance = 365;
            this.mainGameSplitContainer.TabIndex = 2;
            // 
            // arenaMovePanel
            // 
            this.arenaMovePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.arenaMovePanel.IsSplitterFixed = true;
            this.arenaMovePanel.Location = new System.Drawing.Point(0, 0);
            this.arenaMovePanel.Name = "arenaMovePanel";
            this.arenaMovePanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // arenaMovePanel.Panel1
            // 
            this.arenaMovePanel.Panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.arenaMovePanel.Panel1.Controls.Add(this.arenaGroupBox);
            // 
            // arenaMovePanel.Panel2
            // 
            this.arenaMovePanel.Panel2.Controls.Add(this.MovementsGroupBox);
            this.arenaMovePanel.Size = new System.Drawing.Size(365, 454);
            this.arenaMovePanel.SplitterDistance = 276;
            this.arenaMovePanel.TabIndex = 0;
            // 
            // arenaGroupBox
            // 
            this.arenaGroupBox.Controls.Add(this.arenaVisualPanel);
            this.arenaGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.arenaGroupBox.Location = new System.Drawing.Point(5, 3);
            this.arenaGroupBox.Name = "arenaGroupBox";
            this.arenaGroupBox.Size = new System.Drawing.Size(350, 266);
            this.arenaGroupBox.TabIndex = 0;
            this.arenaGroupBox.TabStop = false;
            this.arenaGroupBox.Text = "Arena";
            // 
            // arenaVisualPanel
            // 
            this.arenaVisualPanel.Location = new System.Drawing.Point(6, 24);
            this.arenaVisualPanel.Name = "arenaVisualPanel";
            this.arenaVisualPanel.Size = new System.Drawing.Size(337, 235);
            this.arenaVisualPanel.TabIndex = 0;
            this.arenaVisualPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.arenaVisualPanel_Paint);
            // 
            // MovementsGroupBox
            // 
            this.MovementsGroupBox.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.MovementsGroupBox.Controls.Add(this.moveBtnLeft);
            this.MovementsGroupBox.Controls.Add(this.moveBtnRight);
            this.MovementsGroupBox.Controls.Add(this.moveBtnDown);
            this.MovementsGroupBox.Controls.Add(this.moveBtnUp);
            this.MovementsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MovementsGroupBox.Location = new System.Drawing.Point(5, 3);
            this.MovementsGroupBox.Name = "MovementsGroupBox";
            this.MovementsGroupBox.Size = new System.Drawing.Size(350, 163);
            this.MovementsGroupBox.TabIndex = 0;
            this.MovementsGroupBox.TabStop = false;
            this.MovementsGroupBox.Text = "Movements";
            // 
            // moveBtnLeft
            // 
            this.moveBtnLeft.Location = new System.Drawing.Point(51, 69);
            this.moveBtnLeft.Name = "moveBtnLeft";
            this.moveBtnLeft.Size = new System.Drawing.Size(75, 28);
            this.moveBtnLeft.TabIndex = 3;
            this.moveBtnLeft.Text = "Left";
            this.moveBtnLeft.UseVisualStyleBackColor = true;
            this.moveBtnLeft.Click += new System.EventHandler(this.moveBtnLeft_Click);
            // 
            // moveBtnRight
            // 
            this.moveBtnRight.Location = new System.Drawing.Point(221, 69);
            this.moveBtnRight.Name = "moveBtnRight";
            this.moveBtnRight.Size = new System.Drawing.Size(75, 28);
            this.moveBtnRight.TabIndex = 2;
            this.moveBtnRight.Text = "Right";
            this.moveBtnRight.UseVisualStyleBackColor = true;
            this.moveBtnRight.Click += new System.EventHandler(this.moveBtnRight_Click);
            // 
            // moveBtnDown
            // 
            this.moveBtnDown.Location = new System.Drawing.Point(138, 112);
            this.moveBtnDown.Name = "moveBtnDown";
            this.moveBtnDown.Size = new System.Drawing.Size(75, 28);
            this.moveBtnDown.TabIndex = 1;
            this.moveBtnDown.Text = "Down";
            this.moveBtnDown.UseVisualStyleBackColor = true;
            this.moveBtnDown.Click += new System.EventHandler(this.moveBtnDown_Click);
            // 
            // moveBtnUp
            // 
            this.moveBtnUp.Location = new System.Drawing.Point(138, 25);
            this.moveBtnUp.Name = "moveBtnUp";
            this.moveBtnUp.Size = new System.Drawing.Size(75, 28);
            this.moveBtnUp.TabIndex = 0;
            this.moveBtnUp.Text = "Up";
            this.moveBtnUp.UseVisualStyleBackColor = true;
            this.moveBtnUp.Click += new System.EventHandler(this.moveBtnUp_Click);
            // 
            // inventoryCharSplitPanel
            // 
            this.inventoryCharSplitPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inventoryCharSplitPanel.IsSplitterFixed = true;
            this.inventoryCharSplitPanel.Location = new System.Drawing.Point(0, 0);
            this.inventoryCharSplitPanel.Name = "inventoryCharSplitPanel";
            this.inventoryCharSplitPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // inventoryCharSplitPanel.Panel1
            // 
            this.inventoryCharSplitPanel.Panel1.Controls.Add(this.actionCharPicSplitContainer);
            // 
            // inventoryCharSplitPanel.Panel2
            // 
            this.inventoryCharSplitPanel.Panel2.Controls.Add(this.eventsGroupBox);
            this.inventoryCharSplitPanel.Size = new System.Drawing.Size(590, 454);
            this.inventoryCharSplitPanel.SplitterDistance = 203;
            this.inventoryCharSplitPanel.TabIndex = 0;
            // 
            // actionCharPicSplitContainer
            // 
            this.actionCharPicSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionCharPicSplitContainer.IsSplitterFixed = true;
            this.actionCharPicSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.actionCharPicSplitContainer.Name = "actionCharPicSplitContainer";
            // 
            // actionCharPicSplitContainer.Panel1
            // 
            this.actionCharPicSplitContainer.Panel1.Controls.Add(this.actionsGroupBox);
            // 
            // actionCharPicSplitContainer.Panel2
            // 
            this.actionCharPicSplitContainer.Panel2.Controls.Add(this.InventoryGroupBox);
            this.actionCharPicSplitContainer.Size = new System.Drawing.Size(590, 203);
            this.actionCharPicSplitContainer.SplitterDistance = 149;
            this.actionCharPicSplitContainer.TabIndex = 0;
            // 
            // actionsGroupBox
            // 
            this.actionsGroupBox.Controls.Add(this.actionBtnEquip);
            this.actionsGroupBox.Controls.Add(this.actionBtnDrop);
            this.actionsGroupBox.Controls.Add(this.actionBtbStats);
            this.actionsGroupBox.Controls.Add(this.actionBtnInteract);
            this.actionsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.actionsGroupBox.Location = new System.Drawing.Point(7, 4);
            this.actionsGroupBox.Name = "actionsGroupBox";
            this.actionsGroupBox.Size = new System.Drawing.Size(132, 193);
            this.actionsGroupBox.TabIndex = 0;
            this.actionsGroupBox.TabStop = false;
            this.actionsGroupBox.Text = "Actions";
            // 
            // actionBtnEquip
            // 
            this.actionBtnEquip.Location = new System.Drawing.Point(12, 149);
            this.actionBtnEquip.Name = "actionBtnEquip";
            this.actionBtnEquip.Size = new System.Drawing.Size(108, 27);
            this.actionBtnEquip.TabIndex = 3;
            this.actionBtnEquip.Text = "Equip";
            this.actionBtnEquip.UseVisualStyleBackColor = true;
            // 
            // actionBtnDrop
            // 
            this.actionBtnDrop.Location = new System.Drawing.Point(12, 110);
            this.actionBtnDrop.Name = "actionBtnDrop";
            this.actionBtnDrop.Size = new System.Drawing.Size(108, 27);
            this.actionBtnDrop.TabIndex = 2;
            this.actionBtnDrop.Text = "Drop Item";
            this.actionBtnDrop.UseVisualStyleBackColor = true;
            // 
            // actionBtbStats
            // 
            this.actionBtbStats.Location = new System.Drawing.Point(12, 70);
            this.actionBtbStats.Name = "actionBtbStats";
            this.actionBtbStats.Size = new System.Drawing.Size(108, 27);
            this.actionBtbStats.TabIndex = 1;
            this.actionBtbStats.Text = "Char Stats";
            this.actionBtbStats.UseVisualStyleBackColor = true;
            this.actionBtbStats.Click += new System.EventHandler(this.actionBtbStats_Click);
            // 
            // actionBtnInteract
            // 
            this.actionBtnInteract.Location = new System.Drawing.Point(12, 32);
            this.actionBtnInteract.Name = "actionBtnInteract";
            this.actionBtnInteract.Size = new System.Drawing.Size(108, 27);
            this.actionBtnInteract.TabIndex = 0;
            this.actionBtnInteract.Text = "Interact";
            this.actionBtnInteract.UseVisualStyleBackColor = true;
            this.actionBtnInteract.Click += new System.EventHandler(this.actionBtnInteract_Click);
            // 
            // InventoryGroupBox
            // 
            this.InventoryGroupBox.Controls.Add(this.inventoryListBox);
            this.InventoryGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InventoryGroupBox.Location = new System.Drawing.Point(3, 4);
            this.InventoryGroupBox.Name = "InventoryGroupBox";
            this.InventoryGroupBox.Size = new System.Drawing.Size(427, 193);
            this.InventoryGroupBox.TabIndex = 0;
            this.InventoryGroupBox.TabStop = false;
            this.InventoryGroupBox.Text = "Inventory";
            // 
            // inventoryListBox
            // 
            this.inventoryListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inventoryListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inventoryListBox.FormattingEnabled = true;
            this.inventoryListBox.ItemHeight = 16;
            this.inventoryListBox.Items.AddRange(new object[] {
            "Flashlight"});
            this.inventoryListBox.Location = new System.Drawing.Point(3, 22);
            this.inventoryListBox.Name = "inventoryListBox";
            this.inventoryListBox.Size = new System.Drawing.Size(421, 168);
            this.inventoryListBox.TabIndex = 0;
            // 
            // eventsGroupBox
            // 
            this.eventsGroupBox.Controls.Add(this.messageZoneTextBox);
            this.eventsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eventsGroupBox.Location = new System.Drawing.Point(7, 3);
            this.eventsGroupBox.Name = "eventsGroupBox";
            this.eventsGroupBox.Size = new System.Drawing.Size(575, 236);
            this.eventsGroupBox.TabIndex = 4;
            this.eventsGroupBox.TabStop = false;
            this.eventsGroupBox.Text = "Event Window";
            // 
            // messageZoneTextBox
            // 
            this.messageZoneTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messageZoneTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageZoneTextBox.Location = new System.Drawing.Point(3, 22);
            this.messageZoneTextBox.Name = "messageZoneTextBox";
            this.messageZoneTextBox.ReadOnly = true;
            this.messageZoneTextBox.Size = new System.Drawing.Size(569, 211);
            this.messageZoneTextBox.TabIndex = 0;
            this.messageZoneTextBox.Text = "";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(959, 483);
            this.shapeContainer1.TabIndex = 3;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.Location = new System.Drawing.Point(0, 0);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(75, 23);
            // 
            // MainWindsFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(959, 483);
            this.Controls.Add(this.mainGameSplitContainer);
            this.Controls.Add(this.gameStaticMenuStrip);
            this.Controls.Add(this.shapeContainer1);
            this.MainMenuStrip = this.gameStaticMenuStrip;
            this.MinimizeBox = false;
            this.Name = "MainWindsFrame";
            this.Text = "The Winds Of Change";
            this.Load += new System.EventHandler(this.MainWindsFrame_Load);
            this.gameStaticMenuStrip.ResumeLayout(false);
            this.gameStaticMenuStrip.PerformLayout();
            this.mainGameSplitContainer.Panel1.ResumeLayout(false);
            this.mainGameSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainGameSplitContainer)).EndInit();
            this.mainGameSplitContainer.ResumeLayout(false);
            this.arenaMovePanel.Panel1.ResumeLayout(false);
            this.arenaMovePanel.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.arenaMovePanel)).EndInit();
            this.arenaMovePanel.ResumeLayout(false);
            this.arenaGroupBox.ResumeLayout(false);
            this.MovementsGroupBox.ResumeLayout(false);
            this.inventoryCharSplitPanel.Panel1.ResumeLayout(false);
            this.inventoryCharSplitPanel.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.inventoryCharSplitPanel)).EndInit();
            this.inventoryCharSplitPanel.ResumeLayout(false);
            this.actionCharPicSplitContainer.Panel1.ResumeLayout(false);
            this.actionCharPicSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.actionCharPicSplitContainer)).EndInit();
            this.actionCharPicSplitContainer.ResumeLayout(false);
            this.actionsGroupBox.ResumeLayout(false);
            this.InventoryGroupBox.ResumeLayout(false);
            this.eventsGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void MainWindsFrame_Load(object sender, System.EventArgs e) {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.MenuStrip gameStaticMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveGameAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator gameMenuStripDropdown;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.SplitContainer mainGameSplitContainer;
        private System.Windows.Forms.SplitContainer arenaMovePanel;
        private System.Windows.Forms.GroupBox MovementsGroupBox;
        private System.Windows.Forms.Button moveBtnUp;
        private System.Windows.Forms.Button moveBtnLeft;
        private System.Windows.Forms.Button moveBtnRight;
        private System.Windows.Forms.Button moveBtnDown;
        private System.Windows.Forms.GroupBox arenaGroupBox;
        private System.Windows.Forms.SplitContainer actionCharPicSplitContainer;
        private System.Windows.Forms.GroupBox actionsGroupBox;
        private System.Windows.Forms.SplitContainer inventoryCharSplitPanel;
        private System.Windows.Forms.GroupBox InventoryGroupBox;
        private System.Windows.Forms.Button actionBtnInteract;
        private System.Windows.Forms.Button actionBtbStats;
        private System.Windows.Forms.Button actionBtnEquip;
        private System.Windows.Forms.Button actionBtnDrop;
        private System.Windows.Forms.ListBox inventoryListBox;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private System.Windows.Forms.Panel arenaVisualPanel;
        private System.Windows.Forms.GroupBox eventsGroupBox;
        private System.Windows.Forms.RichTextBox messageZoneTextBox;
    }
}

