﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    public class Sword : Weapon{

	    private String[,] swords = {{"Side Sword","Reverse Dagger","Wide Sword"},
								     {"Bone Sword","Reverse Bone Sword","Steel Broad Sword"},
								     {"Katana","Scythe","Heavy Axe"},
								     {"Exultor","Ascthum","Guthrim"},
								     {"Legendary Exohn","Legendary Ixiom","Legendary AshipHuur"}
								    }; //
	
	    private int[,] swordAttacks = {{110,150,90},
									    {140,180,120},
									    {180,220,160},
									    {250,280,230},
									    {300,320,290}
								       };
	
	    private int[] changingLevels = {10,15,25,35,50};
	
	    private String currentSword;
	    private int currentSwordAttack;
	    private int[] swordPos = new int[2];
	
	
	    public Sword(String classType) {
		    validateCharUsingWeapon(classType);
		
				
	    }
	
	    private void validateCharUsingWeapon(String charT) {
		    if(charT.Equals("assasin")) {
			    currentSword = swords[0, 1];
			    currentSwordAttack = swordAttacks[0, 1];
			    swordPos[0] = 0;
			    swordPos[1] = 1;
		    }else if(charT.Equals("elite")) {
			    currentSword = swords[0, 0];
			    currentSwordAttack = swordAttacks[0, 0];
			    swordPos[0] = 0;
			    swordPos[1] = 0;
		    }else if(charT.Equals("guardian")) {
			    currentSword = swords[0, 2];
			    currentSwordAttack = swordAttacks[0, 2];
			    swordPos[0] = 0;
			    swordPos[1] = 2;
		    }
	    }
		
	    public String getSwordName() {
		    return currentSword;
	    }
	
	    public void wpnLvlUp() {
		    gearLvl++;
		    addWepStr(1);
		
		    for(int i=0; i<changingLevels.Length; i++) {
			    if(gearLvl>=changingLevels[i]) {
				    swordPos[0]++;
				    currentSwordAttack = swordAttacks[swordPos[0], swordPos[1]];
			    }
		    }
			
		    setWepAttack(currentSwordAttack+attackPowerCalculator(getWepStr(), "GEAR"));
				
	    }
    }
}
