﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    public class GameItems {

        //Item name array
	    private String[] possibleItem = {"Large Potion","Small Potion","Strength Stone","Defense Stone","Stealth Stone","Attack Stone","Premonition Stone"};
	
	    //Item Stat Member Vars
	    private int lgPot = 500;
	    private int smPot = 250;
	    private int strStone = 5;
	    private int defStone = 5;
	    private int stlthStone = 5;
	    private int atkStone = 5;
		
	
	    //Constructs --------------------------------------
	    public GameItems() {
		
	    } //end construct

        // =================================================




	    //Private Functions --------------------------------
	    private int generateRandItem() {
		    Random r = new Random((int) DateTime.Now.Ticks & 0x0000FFFF);
		    int rand = r.Next(6);

		    return rand;

	    } //end function
        	
	
	
	
	    //Mutators ------------------------------------------
	    public int getLgPotStat() {
		    return lgPot;

	    } //end function
	
	    public int getSmPotStat() {
		    return smPot;

	    } //end function
	
	    public int getStrStone() {
		    return strStone;

	    } //end function
	
	    public int getDefStone() {
		    return defStone;

	    } //end function
	
	    public int getStealthStone() {
		    return stlthStone;

	    } //end function
	
	    public int getAttackStone() {
		    return atkStone;

	    } //end function
	
	    public String getSpecificItem(int num) {
		    return possibleItem[num];

	    } //end function
	
	    public String getRandItem() {
		    return possibleItem[generateRandItem()];

	    } //end function
	

    } //EOC

} //EON
