﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    
    public class Assasin : NewCharacter  {

	    private static String className = "assasin";
	
	    //Specific char stats
	    private static int strA = 7;
	    private static int defA = 2;
	    private int HPA = 250;
	    private static int stealthA = 10;
	    private static int attackA = 200;
	
	    //Weapons
	    Sword sword;
	
	    //Armor
	    Helmet helmet;
	    BodyArmor bodyArmor;
	    Gloves gloves;
	    Leggings legs;
	    Boots boots;
			
	
        public Assasin() : base(className,strA,defA,stealthA,attackA) {
            this.addHP(HPA);
            this.setCurrentGameHp(this.getHP());
            this.setName(className);
		
        }
	
        public void equipSword() {
            addAtk(sword.equipSword());
		
        }
		
        public void makeSword() {
            sword = new Sword(className);
        }
	
        public void equipHelmet() {
            addToBlock(helmet.getArmorBlock());
            addHP(helmet.getArmorHP());
        }
		
        public void makeHelmet() {
            helmet = new Helmet(className);
        }
	
        public void equipBodyArmor() {
            addToBlock(bodyArmor.getArmorBlock());
            addHP(bodyArmor.getArmorHP());
        }
	
        public void makeBodyArmor() {
            bodyArmor = new BodyArmor(className);
        }
	
        public void equipGloves() {
            addToBlock(gloves.getArmorBlock());
            addHP(gloves.getArmorHP());
        }
	
        public void makeGloves() {
            gloves = new Gloves(className);
        }
	
        public void equipLegs() {
            addToBlock(legs.getArmorBlock());
            addHP(legs.getArmorHP());
        }
	
        public void makeLegs() {
            legs = new Leggings(className);
        }
	
        public void equipBoots() {
            addToBlock(boots.getArmorBlock());
            addHP(boots.getArmorHP());
        }
	
        public void makeBoots() {
            boots = new Boots(className);
        }
	
	
    } //EOC

} //EON
