﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace prog2500ass1 {

    public class PhysicalRoom : Room {
                
        private Rectangle visualRoom;
        private SolidBrush blackBrush = new SolidBrush(Color.Black);
        private SolidBrush whiteBrush = new SolidBrush(Color.White);
        private String roomColor;
        private int width = 45;
        private int height = 40;
        private int x = 0;
        private int y = 0;



        //Default Constructor -----------------------------
        public PhysicalRoom() {            
            this.setRoomColorBlack();

        } //end constructor

        //Overloaded Constructor
        public PhysicalRoom(int newX, int newY) { 
            this.setRoomColorBlack();
            this.setX(newX);
            this.setY(newY);
        
        } //end constructor

        // ================================================




        //Setters -----------------------------------------
        public void setWidth(int newWidth) {
            this.width = newWidth;

        } //end function

        public void setheight(int newHeight) {
            this.height = newHeight;

        } //end function

        public void setX(int newX) {
            this.x = newX;

        } //end function

         public void setY(int newY) {
            this.y = newY;

        } //end function

        public void setRoomColorBlack() {
            this.roomColor = "black";

        } //end function

        public void setRoomColorWhite() {
            this.roomColor = "white";

        } //end function

        





        //Getters -----------------------------------------
        public int getWidth() {
            return this.width;

        } //end function

        public int getHeight() {
            return this.height;

        } //end function

         public int getX() {
            return this.x;

        } //end function
        
        public int getY() {
            return this.y;

        } //end function

        public String getRoomColor() {
            return this.roomColor;

        } //end function
        



        //Functions ---------------------------------------
        public void drawRoom(System.Windows.Forms.PaintEventArgs e, String chosenColor) {
            if(chosenColor.Equals("black")) {
                this.setRoomColorBlack();
                this.visualRoom = new Rectangle(this.x, this.y, this.width, this.height);
                e.Graphics.FillRectangle(this.blackBrush, this.visualRoom);

            } else if(chosenColor.Equals("white")) {
                this.setRoomColorWhite();
                this.visualRoom = new Rectangle(this.x, this.y, this.width, this.height);
                e.Graphics.FillRectangle(this.whiteBrush, this.visualRoom);

            }
        
        } //end function



    } //EOC

} //EON
