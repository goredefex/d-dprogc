﻿namespace prog2500ass1 {
    partial class CharStatsFrame {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.charStatTitleLabel = new System.Windows.Forms.Label();
            this.charTypeLabel = new System.Windows.Forms.Label();
            this.charStatsLvlLabel = new System.Windows.Forms.Label();
            this.strengthLabel = new System.Windows.Forms.Label();
            this.defenseLabel = new System.Windows.Forms.Label();
            this.attackLabel = new System.Windows.Forms.Label();
            this.HPLabel = new System.Windows.Forms.Label();
            this.stealthLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::prog2500ass1.Properties.Resources.allArm;
            this.pictureBox1.Location = new System.Drawing.Point(242, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(166, 220);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // charStatTitleLabel
            // 
            this.charStatTitleLabel.AutoSize = true;
            this.charStatTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.charStatTitleLabel.Location = new System.Drawing.Point(63, 12);
            this.charStatTitleLabel.Name = "charStatTitleLabel";
            this.charStatTitleLabel.Size = new System.Drawing.Size(122, 20);
            this.charStatTitleLabel.TabIndex = 1;
            this.charStatTitleLabel.Text = "Stats and Gear:";
            // 
            // charTypeLabel
            // 
            this.charTypeLabel.AutoSize = true;
            this.charTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.charTypeLabel.Location = new System.Drawing.Point(12, 42);
            this.charTypeLabel.Name = "charTypeLabel";
            this.charTypeLabel.Size = new System.Drawing.Size(83, 17);
            this.charTypeLabel.TabIndex = 2;
            this.charTypeLabel.Text = "Char Type -";
            // 
            // charStatsLvlLabel
            // 
            this.charStatsLvlLabel.AutoSize = true;
            this.charStatsLvlLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.charStatsLvlLabel.Location = new System.Drawing.Point(12, 70);
            this.charStatsLvlLabel.Name = "charStatsLvlLabel";
            this.charStatsLvlLabel.Size = new System.Drawing.Size(55, 17);
            this.charStatsLvlLabel.TabIndex = 3;
            this.charStatsLvlLabel.Text = "Level - ";
            // 
            // strengthLabel
            // 
            this.strengthLabel.AutoSize = true;
            this.strengthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.strengthLabel.Location = new System.Drawing.Point(12, 126);
            this.strengthLabel.Name = "strengthLabel";
            this.strengthLabel.Size = new System.Drawing.Size(71, 17);
            this.strengthLabel.TabIndex = 4;
            this.strengthLabel.Text = "Strength -";
            // 
            // defenseLabel
            // 
            this.defenseLabel.AutoSize = true;
            this.defenseLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defenseLabel.Location = new System.Drawing.Point(12, 153);
            this.defenseLabel.Name = "defenseLabel";
            this.defenseLabel.Size = new System.Drawing.Size(70, 17);
            this.defenseLabel.TabIndex = 5;
            this.defenseLabel.Text = "Defense -";
            // 
            // attackLabel
            // 
            this.attackLabel.AutoSize = true;
            this.attackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attackLabel.Location = new System.Drawing.Point(12, 179);
            this.attackLabel.Name = "attackLabel";
            this.attackLabel.Size = new System.Drawing.Size(56, 17);
            this.attackLabel.TabIndex = 6;
            this.attackLabel.Text = "Attack -";
            // 
            // HPLabel
            // 
            this.HPLabel.AutoSize = true;
            this.HPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPLabel.Location = new System.Drawing.Point(12, 98);
            this.HPLabel.Name = "HPLabel";
            this.HPLabel.Size = new System.Drawing.Size(36, 17);
            this.HPLabel.TabIndex = 7;
            this.HPLabel.Text = "HP -";
            // 
            // stealthLabel
            // 
            this.stealthLabel.AutoSize = true;
            this.stealthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stealthLabel.Location = new System.Drawing.Point(12, 206);
            this.stealthLabel.Name = "stealthLabel";
            this.stealthLabel.Size = new System.Drawing.Size(61, 17);
            this.stealthLabel.TabIndex = 8;
            this.stealthLabel.Text = "Stealth -";
            // 
            // groupBox1
            // 
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 244);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(395, 178);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Weapons and Gear:";
            // 
            // CharStatsFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 434);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.stealthLabel);
            this.Controls.Add(this.HPLabel);
            this.Controls.Add(this.attackLabel);
            this.Controls.Add(this.defenseLabel);
            this.Controls.Add(this.strengthLabel);
            this.Controls.Add(this.charStatsLvlLabel);
            this.Controls.Add(this.charTypeLabel);
            this.Controls.Add(this.charStatTitleLabel);
            this.Controls.Add(this.pictureBox1);
            this.Name = "CharStatsFrame";
            this.Text = "Winds of Change - Character Stats ";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label charStatTitleLabel;
        private System.Windows.Forms.Label charTypeLabel;
        private System.Windows.Forms.Label charStatsLvlLabel;
        private System.Windows.Forms.Label strengthLabel;
        private System.Windows.Forms.Label defenseLabel;
        private System.Windows.Forms.Label attackLabel;
        private System.Windows.Forms.Label HPLabel;
        private System.Windows.Forms.Label stealthLabel;
        private System.Windows.Forms.GroupBox groupBox1;

    }
}