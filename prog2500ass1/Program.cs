﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prog2500ass1 {
    static class Program {

        ///Entry point ==========================================

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindsFrame());

        } //end entry point


        // ======================================================

    }
}
