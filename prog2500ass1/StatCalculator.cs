﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace prog2500ass1 {
    public abstract class StatCalculator {

	
	    public double[] lvlModifier = new double[100];
	    public int[] lvlHitModifier = new int[12];
	    public int level = 1;
	    public int gearLvl = 1;
		
	    bool isEquipped = false;
		

        //Default Constructor -----------------------------
	    public StatCalculator() {
		    fillModifierArray();
		    fillHitModifierArray();
		
	    } //end constructor

        // ================================================
	
	
	    private void fillModifierArray() {
		    int baseNum = 1;
		    double control = 1.3;
		
		    for(int i=0; i<lvlModifier.Length; i++) {
			    lvlModifier[i] = baseNum * control;
		    }

	    }  //end function
	

	    private void fillHitModifierArray() {
		    int baseNum = 45;
		    int control = 2;
			
			
		    for(int i=0; i<lvlHitModifier.Length; i++) {
			    lvlHitModifier[i] = baseNum;
					    baseNum += control;
		    } 

	    } //end function

	
	    public int calculateChanceOfHit() {
		    double chanceHit = 0;
		    int cleanedChanceHit = 0;
								
		    for(int i=0; i<lvlHitModifier.Length; i++) {
			    if(level>=(i * 3)+5) {
				    chanceHit = lvlHitModifier[i];
				    break;
			    }else {
				    chanceHit = 40;
			    }//end if
		    }//end for
		
		    cleanedChanceHit = (int)Math.Round(chanceHit);
				
		    return cleanedChanceHit;

	    } //end function

		
	    public int attackPowerCalculator(int inStr,String lvlType) {
		    double pow = 0;
		    int cleanedPow = 0;
		    int lvlImposter = 0;
		
		    if(lvlType.ToUpper().Equals("GEAR")) {
			    lvlImposter = gearLvl;
		    }else if(lvlType.ToUpper().Equals("PLAYER")) {
			    lvlImposter = level;
		    }
				
		    for(int i=0; i<lvlModifier.Length; i++) {
			    if(lvlImposter==(i * 3)+5) {
				    pow = lvlImposter * lvlModifier[i];
								
			    }//end if
		    }//end for
		
		    cleanedPow = (int)Math.Round(addStrCalcToAttack(inStr,pow));
				
		    return cleanedPow;

	    } //end function

	
	    public double addStrCalcToAttack(int inStr,double pow) {
		    int strModifier = 10;
		    int newStr = strModifier * inStr;
		    pow += newStr;
		
		    return pow;

	    } //end function

	
	    public int defenseCalculator(int inDef,String lvlType) {
		    double def = 0;
		    int cleanedDef = 0;
		    int lvlImposter = 0;
		
		    if(lvlType.ToUpper().Equals("GEAR")) {
			    lvlImposter = gearLvl;
		    }else if(lvlType.ToUpper().Equals("PLAYER")) {
			    lvlImposter = level;
		    }
		
					
		    for(int i=0; i<lvlModifier.Length; i++) {
			    if(lvlImposter==(i * 3)+5) {
				    def = lvlImposter * lvlModifier[i];
				    break;					
			    }//end if
		    }//end for
			
		    cleanedDef = (int)Math.Round(addDefCalcToBlock(inDef,def));
					
		    return cleanedDef;

	    } //end function

	
	    public double addDefCalcToBlock(int inDef,double def) {
		    int defModifier = 10;
		    int newDef = defModifier * inDef;
		    def += newDef;
		
		    return def;

	    } //end function
	

	    public bool getIsEquipped() {
		    return isEquipped;

	    } //end function

	
	    public int calculateChanceOfEvade(int inStealth, String lvlType) {
		    double chanceHit = 0;
		    int cleanedChanceHit = 0;
		    int lvlImposter = 0;
		
		    if(lvlType.ToUpper().Equals("GEAR")) {
			    lvlImposter = gearLvl;
		    }else if(lvlType.ToUpper().Equals("PLAYER")) {
			    lvlImposter = level;
		    }
		
					
		    for(int i=0; i<lvlModifier.Length; i++) {
			    if(lvlImposter==(i * 3)+5) {
				    chanceHit = lvlImposter * lvlModifier[i];
									
			    }//end if
		    }//end for
			
		    cleanedChanceHit = (int)Math.Round(addStealthCalcToEvade(inStealth,chanceHit));
					
		    return cleanedChanceHit;

	    } //end function
	

	    public double addStealthCalcToEvade(int inStealth,double evade) {
		    int stlthModifier = 10;
		    int newStealth = stlthModifier * inStealth;
		    evade += newStealth;
		
		    return evade;

	    } //end function
	
	
	
    } //EOC

} ///EON
