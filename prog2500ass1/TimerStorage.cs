﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace prog2500ass1 {
    public class TimerStorage {

        //Member Vars
        private long DELAY = 0;
	    private double tLength = 50;
	    private int eventCounter = 0;
        
        //Timer Control Object
	    private Timer eventChanceTimer;
	
		
        //Constructs ----------------------------------------
	    public TimerStorage() {
		    this.eventChanceTimer = new Timer(this.tLength);
            this.eventChanceTimer.Elapsed += eventChanceTimer_Elapsed;
            this.eventChanceTimer.Start();
		
	    } //end construct

        // ==================================================
        	


        //Getters -------------------------------------------
	    public Timer getEventTimer() {
		    return eventChanceTimer;

	    } //end function
	
	    public int getCounter() {
		    return eventCounter;

	    } //end function


        //Timer Event Handler
        private void eventChanceTimer_Elapsed(object sender, ElapsedEventArgs e) {
            eventCounter++;
		
		    if(eventCounter == 100) {
			    eventCounter = 0;

		    }//End if

        } //end event



    } //EOC

} //EON
